from itertools import product, combinations, permutations
from collections import deque

def load_dictionary(filepath):
    """

    Used for reading words from a path and selecting only 3 letter words from the list.
    :param filepath: default 'usr/share/dict/words'
    :return: list with all the 3-letter words

    """
    all_words = open(file=filepath, mode='r')
    words_merged = all_words.read()
    words = words_merged.split('\n')
    letter3_words = ['p']
    letter3_words = [word.lower() for word in words if len(word) == 3 and word not in letter3_words]
    return letter3_words

def generate_three_letter_words(**kwargs):
    """

    :param word_list: Word list of all the legal words present in the dictionary
    :param kwargs: lists from which iterable variables are taken
    :return: list with all 3 letter legal words

    """
    temp_word_list = [''.join(letters).lower() for letters in product(kwargs['first'], kwargs['second'], kwargs['third'])]
    return temp_word_list

def generate_all_list_combination(**kwargs):
    """
    Function to generate all the possible combinations of first, second and third rows
    :param word_list: list of legal words that have to be sent for reference
    :param kwargs: lists from which iterable variables are taken
    :return: all possible 3 lettered legal word combinations

    """
    keyboard = [kwargs['first_row'],
                kwargs['second_row'],
                kwargs['third_row']]

    queue_number = [0,1,2]
    all_words_list = []
    combinations_all = [list(p) for p in permutations(iterable=queue_number)]

    for index, value in enumerate(combinations_all):
        print(index, value)
        temp_word_list = generate_three_letter_words(first=keyboard[value[0]],
                                                     second=keyboard[value[1]],
                                                     third=keyboard[value[2]])
        for word in temp_word_list:
            all_words_list.append(word)

    return all_words_list


if __name__ == '__main__':
    # Definition of constants
    FIRST_ROW = ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P']
    SECOND_ROW = ['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L']
    THIRD_ROW = ['Z', 'X', 'C', 'V', 'B', 'N', 'M']

    word_list = load_dictionary('/usr/share/dict/words')
    all_words_list = generate_all_list_combination(first_row=FIRST_ROW, second_row=SECOND_ROW, third_row=THIRD_ROW)
    correct_words = [word for word in all_words_list if word in word_list]
    print(correct_words)